package tests.com.virunga.billpay.activities;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;

import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.utils.PCGeneralUtils;
import com.virunga.billpay.activities.VRSignupActivity;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by emmy on 09/01/2018.
 */

public class VRSignupActivityTest extends ActivityInstrumentationTestCase2<VRSignupActivity> {

     private static final String CLAZZ = VRSignupActivityTest.class.getName();
     private VRSignupActivity vrSignupActivity;


    public VRSignupActivityTest() {
       super(CLAZZ,VRSignupActivity.class);
    }

      @Before
      public void setUp() throws Exception {
         super.setUp();
         setActivityInitialTouchMode(false);
         vrSignupActivity = getActivity();

      }


    @Test
    public void testOverallMechanism() {
        assertNotNull(vrSignupActivity);
        //TODO: incomplete test case
    }



    @Test
    public void testAllRequiredField() {
        /**
         * Test all required field positive case.
         */
        EditText emailText = (EditText) vrSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.email);
        EditText phoneText = (EditText) vrSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.phone);
        EditText firstNameText = (EditText)vrSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.firstName);
        EditText lastNameText = (EditText) vrSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.lastName);
        EditText pwdText = (EditText) vrSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.password);

        assertNotNull(emailText);
        assertNotNull(phoneText);
        assertNotNull(firstNameText);
        assertNotNull(lastNameText);
        assertNotNull(pwdText);

        /**
         * Test all required field negative caase.
         */

        emailText = null;
        firstNameText = null;

        assertNull(emailText);
        assertNotNull(phoneText);
        assertNull(firstNameText);
        assertNotNull(lastNameText);
        assertNotNull(pwdText);


    }


    @Test
    public void testEmptyFieldValidation() throws PCGenericError {

        try {
            boolean result =  vrSignupActivity.validCustomerInfo("George","Kagabo","john@smith.com","+1 4056667777","");
        } catch (PCGenericError error) {
            assertEquals(error.getMessage(), vrSignupActivity.getResources().getString(com.pesachoice.billpay.activities.R.string.sign_error_missing_info));
        }


        /**
         * Test empty field validation positive case.
         */

        String phone = "+1 4056667777";
        String countryCode = "+1";

        // First check for any empty fields or in wrong formart
        if (PCGeneralUtils.isValidPhone(phone.substring(1), countryCode)) {


            assertTrue(vrSignupActivity.validCustomerInfo("George","Kagabo","john@smith.com","+1 4056667777","emmanuel"));

        }

    }


    @Test
    public void testInvalidPhoneFieldValidation() throws PCGenericError{
        try {
            boolean result =  vrSignupActivity.validCustomerInfo("George","Kagabo","john@smith.com","+1 4056667777","");
        } catch (PCGenericError error) {
            assertEquals(error.getMessage(), vrSignupActivity.getResources().getString(com.pesachoice.billpay.activities.R.string.sign_error_invalid_phone));
        }
        /**
         * Test invalid phone field validation positive case.
         */

        boolean result =  vrSignupActivity.validCustomerInfo("Patrick","Ronald","patrick@ronald.com","+250782901278","emmanuel");
        assertTrue(result);

    }

    @Test
    public void testInvalidEmailFieldValidation() {
        vrSignupActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                EditText emailText = (EditText) vrSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.email);
                EditText phoneText = (EditText) vrSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.phone);
                EditText firstNameText = (EditText) vrSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.firstName);
                EditText lastNameText = (EditText) vrSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.lastName);
                EditText pwdText = (EditText) vrSignupActivity.findViewById(com.pesachoice.billpay.activities.R.id.password);

                emailText.setText("johnsmith.com");
                phoneText.setText("+1 4056667777");
                firstNameText.setText("George");
                lastNameText.setText("Camaro");
                pwdText.setText("test123");

                String first = firstNameText.getText().toString();
                String last = lastNameText.getText().toString();




                try {
                    vrSignupActivity.validCustomerInfo(
                            first, last,
                            emailText.getText().toString(),
                            phoneText.getText().toString(),
                            pwdText.getText().toString()
                    );
                } catch (Exception error) {
                    assertEquals(error.getMessage(), vrSignupActivity.getResources().getString(com.pesachoice.billpay.activities.R.string.sign_error_invalid_email));
                }





                /**
                 * Test invalid email field validation positive case.
                 */

                emailText.setText("john@smith.com");



                try {
                    boolean result = vrSignupActivity.validCustomerInfo(
                            first, last,
                            emailText.getText().toString(),
                            phoneText.getText().toString(),
                            pwdText.getText().toString()
                    );
                    assertTrue(result);
                } catch (Exception error) {
                    error.getMessage();
                }
            }
        });
    }





}
