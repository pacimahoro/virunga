package tests.com.virunga.billpay.activities;

import android.test.ActivityInstrumentationTestCase2;
import android.view.MenuItem;
import com.virunga.billpay.activities.VRAccountDetailsActivity;


import org.junit.Before;
import org.junit.Test;

/**
 * Created by emmy on 09/01/2018.
 */

public class VRAccountDetailsActivityTest extends ActivityInstrumentationTestCase2<VRAccountDetailsActivity> {

      private static final String CLAZZ = VRAccountDetailsActivityTest.class.getName();
      private VRAccountDetailsActivity accountDetailsActivity;

      public VRAccountDetailsActivityTest() {

          super(CLAZZ,VRAccountDetailsActivity.class);
      }

    @Before
    protected void setUp () throws Exception {
           super.setUp();
           setActivityInitialTouchMode(false);
          accountDetailsActivity = getActivity();
      }

      @Test
      public void testOnOptionsItemSelected () {
          MenuItem item = (MenuItem) getActivity();
          Boolean itemSelectedResult = accountDetailsActivity.onOptionsItemSelected(item);
          assertTrue(itemSelectedResult);

      }
}
