package tests.com.virunga.billpay.business.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.pesachoice.billpay.business.PCPesabusClient;
import com.virunga.billpay.business.receiver.VRSignupBroadcastReceiver;

/**
 * Created by emmy on 12/5/17.
 */

  public class VRSignupTestBroadcastReceiver extends BroadcastReceiver {

      private static final String CLAZZ = VRSignupTestBroadcastReceiver.class.getName();
    @Override
    public void onReceive(Context context, Intent intent) {
        //            Uri data = intent.getData();
        Log.v(CLAZZ, "Got the following");
        String type = intent.getStringExtra(PCPesabusClient.EXTRA_ACTION_TYPE);
        Log.v(CLAZZ, "Got the following : [" + type + "]");

    }
}
