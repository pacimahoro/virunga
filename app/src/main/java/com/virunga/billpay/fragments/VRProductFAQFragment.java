package com.virunga.billpay.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import com.virunga.billpay.activities.VRAccountDetailsActivity;
import com.pesachoice.billpay.activities.PCAccountDetailsActivity;
import com.pesachoice.billpay.fragments.PCProductFAQFragment;
import com.pesachoice.billpay.model.PCFAQData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by emmy on 12/18/17.
 */

public class VRProductFAQFragment extends PCProductFAQFragment {

    private ExpandableListAdapter faqAdapter;

    

   @Override
   protected List<PCFAQData> getFaqsAndAnswers(){
        BufferedReader reader = null;
        StringBuffer buffer = new StringBuffer();
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getActivity().getAssets().open("vr_product_faq.json")));

            // do reading, loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                buffer.append(mLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return jsonToFAQList(buffer.toString());
    }


}
