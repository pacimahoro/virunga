package com.virunga.billpay.fragments;

import com.pesachoice.billpay.fragments.PCCountryAndProductAdapter;
import com.pesachoice.billpay.fragments.PCSupportedCountriesFragment;
import com.pesachoice.billpay.model.PCCountryProfile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emmy on 06/02/2018.
 */

public class VRSupportedCountriesFragment extends PCSupportedCountriesFragment {

    public  VRSupportedCountriesFragment () {

    }

    @Override
    public void setupMainView(PCCountryProfile countryProfile) {
        //setup countries list
        List<String> countryNames = new ArrayList<>();
        countryNames.add("Rwanda");
        countryNames.add("Uganda");
        countryNames.add("Kenya");
        List<Integer> images = new ArrayList<>();

        //setup icon
        images.add(com.pesachoice.billpay.activities.R.drawable.icn_rwanda);
        images.add(com.pesachoice.billpay.activities.R.drawable.icn_uganda);
        images.add(com.pesachoice.billpay.activities.R.drawable.icn_kenya);
        if (getActivity() != null) {
            adapter = new PCCountryAndProductAdapter(getActivity(), countryNames, images);
            adapter.setTypeOfDatatToAdapt(TypeOfDataToAdapt.COUNTRIES);
            mainFragmentLayout.setAdapter(adapter);
        }
    }

}
