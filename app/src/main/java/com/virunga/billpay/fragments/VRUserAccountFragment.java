package com.virunga.billpay.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.amulyakhare.textdrawable.TextDrawable;
import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.PCRecommendFriendsActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.fragments.PCCardDialogFragment;
import com.pesachoice.billpay.fragments.PCHelpContactUsFragment;
import com.virunga.billpay.activities.VRAccountDetailsActivity;
import com.pesachoice.billpay.fragments.PCUserAccountFragment;;
import com.pesachoice.billpay.model.PCUser;
import com.virunga.billpay.activities.VRMainTabActivity;
import com.virunga.billpay.activities.VRTakePhotoIdActivity;
import org.springframework.util.StringUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by emmy on 12/20/17.
 */

public class VRUserAccountFragment extends PCUserAccountFragment {
    private Map<String, Integer> optionItems = new LinkedHashMap<>();
    private View userAccountFragmentView;
    private VRMainTabActivity activity;
    private ListView optionsView = null;
    private VROptionsAdapter adapter;

    public VRUserAccountFragment () {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        userAccountFragmentView = inflater.inflate(com.pesachoice.billpay.activities.R.layout.pc_fragment_user_account, container, false);
        activity = (VRMainTabActivity) getActivity();
        optionItems.put(PCPesabusClient.OPTION_MOBILE_NUMBER, com.pesachoice.billpay.activities.R.drawable.icn_call_us);
        optionItems.put(PCPesabusClient.OPTION_REFER_FRIENDS, com.pesachoice.billpay.activities.R.drawable.ic_invite_friends);
        optionItems.put(PCPesabusClient.OPTION_TAKE_PHOTO_ID, com.pesachoice.billpay.activities.R.drawable.ic_take_photo_id);
        optionItems.put(PCPesabusClient.OPTION_PAYMENT, com.pesachoice.billpay.activities.R.drawable.icn_product_payment1);
        optionItems.put(PCPesabusClient.OPTION_CONTACT_US, com.pesachoice.billpay.activities.R.drawable.icn_contact_us);
        optionItems.put(PCPesabusClient.OPTION_LOGOUT, com.pesachoice.billpay.activities.R.drawable.icn_logout);
        if (userAccountFragmentView != null) {
            TextView fullName = (TextView) userAccountFragmentView.findViewById(com.pesachoice.billpay.activities.R.id.full_name);
            TextView email = (TextView) userAccountFragmentView.findViewById(com.pesachoice.billpay.activities.R.id.email);
            optionsView = (ListView) userAccountFragmentView.findViewById(com.pesachoice.billpay.activities.R.id.listView_options);
            VRMainTabActivity mainActivity = (VRMainTabActivity) getActivity();
            PCUser user = mainActivity.getAppUser();
            String fName = "";
            if (user != null) {
                fName = user.getFullName();
                TextDrawable drawable1 = TextDrawable.builder()
                        .beginConfig()
                        .useFont(Typeface.DEFAULT)
                        .fontSize(24)
                        .toUpperCase()
                        .endConfig()
                        .buildRound(getInitial(fName), com.pesachoice.billpay.activities.R.color.sectionBackground);
                ImageView profileImage = (ImageView) userAccountFragmentView.findViewById(com.pesachoice.billpay.activities.R.id.account_profile_image);
                profileImage.setImageDrawable(drawable1);
                fullName.setText(StringUtils.capitalize(fName));
                email.setText(user.getEmail());
            }
            setAdater(makeOption(optionItems));
        }

        return userAccountFragmentView;
    }
    @Override
    protected void setAdater(List<PCOption> options) {
        if (userAccountFragmentView != null) {
            if (optionsView == null) {
                optionsView = (ListView) userAccountFragmentView.findViewById(com.pesachoice.billpay.activities.R.id.listView_options);
            }
            if (options != null) {
                adapter = new VROptionsAdapter((PCBaseActivity) this.getActivity(), options);
                optionsView.setAdapter(adapter);
            }
        }
    }


    private class VROptionsAdapter extends BaseAdapter {

        private PCBaseActivity context = null;
        private LayoutInflater inflater = null;
        private TextView paymentCardField;
        private List<PCOption> options = new ArrayList<>();
        private Fragment frag = new Fragment();

    public VROptionsAdapter(PCBaseActivity context, List<PCUserAccountFragment.PCOption> pcOptions) {
            this.context = context;
            options = pcOptions;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /**
         * How many items are in the data set represented by this Adapter.
         *
         * @return Count of items.
         */
        @Override
        public int getCount() {
            return options.size();
        }

        /**
         * Get the data item associated with the specified position in the data set.
         *
         * @param position Position of the item whose data we want within the adapter's
         *                 data set.
         * @return The data at the specified position.
         */
        @Override
        public Object getItem(int position) {
            return options.get(position);
        }

        /**
         * Get the row id associated with the specified position in the list.
         *
         * @param position The position of the item within the adapter's data set whose row id we want.
         * @return The id of the item at the specified position.
         */
        @Override
        public long getItemId(int position) {
            return 0;
        }

        /**
         * Get a View that displays the data at the specified position in the data set. You can either
         * create a View manually or inflate it from an XML layout file. When the View is inflated, the
         * parent View (GridView, ListView...) will apply default layout parameters unless you use
         * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
         * to specify a root view and to prevent attachment to the root.
         *
         * @param position    The position of the item within the adapter's data set of the item whose view
         *                    we want.
         * @param convertView The old view to reuse, if possible. Note: You should check that this view
         *                    is non-null and of an appropriate type before using. If it is not possible to convert
         *                    this view to display the correct data, this method can create a new view.
         *                    Heterogeneous lists can specify their number of view types, so that this View is
         *                    always of the right type (see {@link #getViewTypeCount()} and
         *                    {@link #getItemViewType(int)}).
         * @param parent      The parent that this view will eventually be attached to
         * @return A View corresponding to the data at the specified position.
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;
            if (rowView == null) {
                rowView = inflater.inflate(com.pesachoice.billpay.activities.R.layout.pc_line_item_option, parent, false);
            }
            final TextView option_name = (TextView) rowView.findViewById(com.pesachoice.billpay.activities.R.id.option_name);
            ImageView optionIcon = (ImageView) rowView.findViewById(com.pesachoice.billpay.activities.R.id.option_icon);
            TextView option_value = (TextView) rowView.findViewById(com.pesachoice.billpay.activities.R.id.option_value);
            final PCUserAccountFragment.PCOption option = (PCUserAccountFragment.PCOption) getItem(position);
            option_name.setText(option.getOptionName());
            option_value.setText(option.getOptionValue());
            optionIcon.setImageResource(option.getOptionIconName());

            rowView.setBackgroundColor(getResources().getColor(com.pesachoice.billpay.activities.R.color.sectionBackground));

            if (PCPesabusClient.OPTION_PAYMENT.equals(option_name.getText())) {
                this.paymentCardField = option_value;
            }

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView textView = (TextView) v.findViewById(com.pesachoice.billpay.activities.R.id.option_name);
                    String optionName = textView.getText().toString();
                    String url = "";

                    if (optionName != null) {
                        if (optionName == PCPesabusClient.OPTION_LOGOUT) {
                            onLogoutAction(null);
                        } else if (optionName == PCPesabusClient.OPTION_PAYMENT) {
                            //flag to detect if we're calling
                            ((PCMainTabActivity) getActivity()).isCallFromUserAccountFragment = true;
                            PCCardDialogFragment dialogFragment = new PCCardDialogFragment();
                            dialogFragment.setCalling_fragment(PCCardDialogFragment.Calling_Fragment.USER_ACCOUNT);
                            FragmentManager fm = getActivity().getSupportFragmentManager();
                            dialogFragment.show(fm, PCPesabusClient.CARD_DIALOG);
                        } else if (optionName == PCPesabusClient.OPTION_MOBILE_NUMBER || optionName == PCPesabusClient.OPTION_CALLING_CREDIT) {
                            //TODO: need to figure out what to do if user clicks mobile
                        } else if (optionName == PCPesabusClient.OPTION_REFER_FRIENDS) {
                            /**
                             * used to show screen to show screen to recommend friends
                             */
                            Intent intent = new Intent(getActivity(), PCRecommendFriendsActivity.class);
                            intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, ((VRMainTabActivity) getActivity()).getAppUser());
                            startActivity(intent);

                        } else if (optionName == PCPesabusClient.OPTION_TAKE_PHOTO_ID) {
                            Intent intent = new Intent(getActivity(), VRTakePhotoIdActivity.class);
                            intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, ((VRMainTabActivity) getActivity()).getAppUser());
                            startActivity(intent);
                        } else if (optionName == PCPesabusClient.OPTION_CONTACT_US) {

                            PCHelpContactUsFragment frag = new PCHelpContactUsFragment();
                            frag.setTransaction(null);
                            FragmentManager fm = context.getSupportFragmentManager();
                            frag.show(fm, "Contact us Dialog fragment");
                        } else if (optionName == PCPesabusClient.OPTION_PRIVACY || optionName == PCPesabusClient.OPTION_TERMS_OF_SERVICES) {
                            Intent intent = new Intent(getActivity(), VRAccountDetailsActivity.class);
                            intent.putExtra(PCPesabusClient.OPTION, optionName);
                            intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, ((VRMainTabActivity) getActivity()).getAppUser());
                            startActivity(intent);
                        }
                    }
                }
            });
            return rowView;
        }




    }

}
