package com.virunga.billpay.business.receiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by emmy on 12/5/17.
 */

public class VRSignupBroadcastReceiver extends BroadcastReceiver {

    private final static String CLAZZ = VRSignupBroadcastReceiver.class.getName();

    private static volatile boolean singletonIsCreated = false;

    private Activity activity;

    private static VRSignupBroadcastReceiver signupBroadcastReceiver = null;

    public static VRSignupBroadcastReceiver createOrGetBroadcastReceiver(Activity activity) {
        if (singletonIsCreated) {
            return signupBroadcastReceiver;
        }
        else {
            singletonIsCreated = true;
            return new VRSignupBroadcastReceiver(activity);
        }
    }



    private VRSignupBroadcastReceiver(Activity activity){
        this.activity = activity;
    }











    @Override
    public void onReceive(Context context, Intent intent) {

    }
}
