package com.virunga.billpay.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.R;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.PCSignupActivity;
import com.pesachoice.billpay.activities.helpers.DismissKeyboardListener;
import com.pesachoice.billpay.business.PCPesabusClient;

import org.springframework.util.StringUtils;

/**
 * Created by emmy on 12/5/17.
 */

public class VRSignupActivity extends PCSignupActivity {

    private static final String CLAZZZ = VRSignupActivity.class.getName();
    private VRSignupActivity activity;
    private EditText  phoneText;

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.vr_activity_signup);

         Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
         setSupportActionBar(toolbar);
         phoneText = (EditText) findViewById(R.id.phone);
         countryPicker1 = (TextView) findViewById(R.id.countryPicker1);
         //set the country code by default based on the user's sim card or wifi they're connected
         selectedCountry = null;
         activity = this;
         String countryCodeValue = this.getUserCountryCode(this);
         selectedCountry = countryCodeValue != null ? countryCodeMap.get(countryCodeValue) : null;
         if (!StringUtils.isEmpty(selectedCountry)) {
             countryPicker1.setText(selectedCountry.substring(selectedCountry.indexOf("+")));
         } else {
             showCountriesCode(activity);
         }

         countryPicker1.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 showCountriesCode(activity);
             }
         });

         EditText phoneText = (EditText) findViewById(R.id.phone);
         phoneText.setOnFocusChangeListener(this);

         EditText passwordText = (EditText) findViewById(R.id.password);
         passwordText.setOnFocusChangeListener(this);

         getSupportActionBar().setDisplayHomeAsUpEnabled(true);

         //set privacy and tos links
         TextView tosAndPrivacyText = (TextView) findViewById(R.id.read_tos_and_privacy);
         tosAndPrivacyText.setMovementMethod(LinkMovementMethod.getInstance());
         // Listen for keyboard dismissal
         findViewById(R.id.signup_container).setOnClickListener(new DismissKeyboardListener(this));

        /*
         * TODO: incomplete
         */
         if (savedInstanceState == null) {
             //TODO: Missing implementation
         }
    }

}
