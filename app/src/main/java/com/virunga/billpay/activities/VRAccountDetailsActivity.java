package com.virunga.billpay.activities;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pesachoice.billpay.fragments.PCActivityDetailFragment;
import com.pesachoice.billpay.fragments.PCCardPaymentFragment;
import com.pesachoice.billpay.fragments.PCContactsFragment;
import com.pesachoice.billpay.fragments.PCMyEventTicketFragment;
import com.pesachoice.billpay.fragments.PCProductFAQFragment;
import com.pesachoice.billpay.fragments.PCReportProblemFragment;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.virunga.billpay.fragments.VRProductFAQFragment;
import com.pesachoice.billpay.activities.PCAccountDetailsActivity;
import com.pesachoice.billpay.business.PCPesabusClient;


/**
 * Created by emmy on 12/18/17.
 */

public class VRAccountDetailsActivity extends PCAccountDetailsActivity {

    private String pesachoiceHelpUrl = "";
    private PCBillPaymentData pcBillPaymentData = null;

    @Override
    protected void setupLayout(Toolbar myToolBar, String optionName) {
        Button sendReport = (Button) myToolBar.findViewById(com.pesachoice.billpay.activities.R.id.button_send);
        TextView title = (TextView) myToolBar.findViewById(com.pesachoice.billpay.activities.R.id.textView_action);
        title.setText(optionName);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        switch (optionName) {
            case PCPesabusClient.OPTION_PAYMENT:
                PCCardPaymentFragment paymentFragment = new PCCardPaymentFragment();
                ft.add(com.pesachoice.billpay.activities.R.id.fragment_container, paymentFragment, PCPesabusClient.OPTION_PAYMENT);
                ft.commit();

                break;
            case PCPesabusClient.OPTION_REPORT_PROBLEM:
                sendReport.setVisibility(View.VISIBLE);
                Fragment reportProblemFragment = new PCReportProblemFragment();
                ft.add(com.pesachoice.billpay.activities.R.id.fragment_container, reportProblemFragment, PCPesabusClient.OPTION_REPORT_PROBLEM);
                ft.commit();
                break;
            case PCPesabusClient.OPTION_HELP:
                VRProductFAQFragment faqsFragment = new VRProductFAQFragment();
                ft.add(com.pesachoice.billpay.activities.R.id.fragment_container, faqsFragment, PCPesabusClient.OPTION_HELP);
                ft.commit();
                break;
            case PCPesabusClient.OPTION_PRIVACY:
                optionName.replaceAll("([a-z])", "");
                title.setVisibility(View.INVISIBLE);
                sendReport.setVisibility(View.INVISIBLE);
                break;
            case PCPesabusClient.OPTION_TERMS_OF_SERVICES:
                optionName.replaceAll("([a-z])", "");
                title.setVisibility(View.INVISIBLE);
                sendReport.setVisibility(View.INVISIBLE);
                break;
            case PCPesabusClient.ACTIVITY_ITEM_CLICK:
                Fragment activityReportFragment = new PCActivityDetailFragment();
                pcBillPaymentData = (PCBillPaymentData) getIntent().getSerializableExtra("activity");
                ft.add(com.pesachoice.billpay.activities.R.id.fragment_container, activityReportFragment, PCPesabusClient.ACTIVITY_ITEM_CLICK);
                ft.commit();
                break;
            case PCPesabusClient.OPTION_CONTACTS_FROM_PHONE:
                PCContactsFragment contactsFragment = new PCContactsFragment();
                ft.add(com.pesachoice.billpay.activities.R.id.fragment_container, contactsFragment, PCPesabusClient.OPTION_CONTACTS_FROM_PHONE);
                ft.commit();
                break;
            case PCPesabusClient.OPTION_SHOW_MY_TICKETS:
                PCMyEventTicketFragment eventTicketFragment = new PCMyEventTicketFragment();
                ft.add(com.pesachoice.billpay.activities.R.id.fragment_container, eventTicketFragment, PCPesabusClient.OPTION_SHOW_MY_TICKETS);
                ft.commit();
                break;
            default:
                break;
        }

    }

}
