package com.virunga.billpay.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.pesachoice.billpay.activities.PCTakePhotoIdActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCSavePhotoMetadataController;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCMetadata;
import com.pesachoice.billpay.model.PCSavePhotoIDMetadataRequest;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.virunga.billpay.utils.VRProfilePictureUtil;
import java.io.ByteArrayOutputStream;

/**
 * Created by emmy on 31/01/2018.
 */

public class VRTakePhotoIdActivity extends PCTakePhotoIdActivity {
  private String photoIdName = "";
  private String userName = null;
  private VRTakePhotoIdActivity cameraActivity = null;
  private PCSpecialUser appUser = null ;
  final static String TAG = "VRTakePhotoIdActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.pesachoice.billpay.activities.R.layout.pc_take_photo_id_activity);
        takePhoto = (Button) findViewById(com.pesachoice.billpay.activities.R.id.take_id_photo);
        cameraActivity = this;
        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object objBundle = bundle.get(PCPesachoiceConstant.USER_INTENT_EXTRA);
                appUser = (PCSpecialUser) objBundle;
                if (appUser != null) {
                    userName = appUser.getUserName();
                }
            }
        }
        if(takePhoto != null) {
            takePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestExternalStoragePermission();
                }

            });

        }

        requestExternalStoragePermission();
    }


    @Override
    protected void sendProfilePicture() {
        try {
            if (mImageBitmap != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                mImageBitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                byte[] byteArray = stream.toByteArray();
                //upload photo to FILEBASE
                VRProfilePictureUtil.initializeBucket(cameraActivity);
                photoIdName = userName + "_" + System.nanoTime();
                VRProfilePictureUtil.uploadPhoto(photoIdName, byteArray);
            }
        } catch (Exception e) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            e.printStackTrace();
        }

    }



   @Override
    protected void sendRequestToSavePhotoIDMetadata(String photo_id_path) {
        try {
            PCSavePhotoIDMetadataRequest request = new PCSavePhotoIDMetadataRequest();
            request.setUser(appUser);
            PCMetadata metadata = new PCMetadata();
            metadata.setName(VRProfilePictureUtil.main_forlder + "/" + photoIdName);
            metadata.setDownloadURL(photo_id_path);
            request.setPhotoMetadata(metadata);
            PCSavePhotoMetadataController savePhotoMetadataController = (PCSavePhotoMetadataController) PCControllerFactory.constructController(
                    PCControllerFactory.PCControllerType.SAVE_PHOTO_METADATA, this);
            if (savePhotoMetadataController != null) {
                savePhotoMetadataController.setActivity(this);
                savePhotoMetadataController.setServiceType(PCPesabusClient.PCServiceType.SAVE_PHOTO_METADATA);
                savePhotoMetadataController.execute(request);
            }
        } catch (Throwable exc) {
            Log.e(TAG, "Could not handle Saving Photo metadata process because [" + exc.getMessage() + "]");
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            PCGenericError error = new PCGenericError();
            if (exc instanceof PCGenericError) {
                error = (PCGenericError)exc;
            }
            else
                error.setMessage(exc.getMessage());
            presentError(error, "Error while saving photo metadata");
        }
    }

}
