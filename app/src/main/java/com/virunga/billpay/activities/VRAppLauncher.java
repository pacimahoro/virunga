package com.virunga.billpay.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.R;
import com.pesachoice.billpay.activities.PCAppLauncher;
import com.pesachoice.billpay.activities.PCSignupActivity;


/**
 * Created by emmy on 12/5/17.
 */

public class VRAppLauncher extends PCAppLauncher {

    private  final static String CLAZZ = VRAppLauncher.class.getName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vr_activity_signup_login);
    }

    @Override
    public void openLogin(View view) {
        Log.d(CLAZZ, "Open login Activity");
        Intent intent = new Intent(this, VRLoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void openSignUp(View view) {
        Log.d(clazzz, "Open Sign Up Activity");
        Intent intent = new Intent(this, PCSignupActivity.class);
        startActivityForResult(intent, SIGNUP_ACTIVITY_REQUEST_CODE);
    }




}